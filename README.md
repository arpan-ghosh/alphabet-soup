# Alphabet Soup Solution

## Introduction
This Word Search Solver is a Python program designed to efficiently find words in a grid of characters in which a selection of words have been hidden. It uses a custom written Ternary Search Trie (TST) data structure for storing and searching words, offering a balance between time and space complexity.

## Features
- Finds words placed in all directions (horizontal, vertical, and diagonal) in a grid.
- Utilizes a Ternary Search Trie for efficient word search.
- Accepts input from a text file containing the grid and words list.

## Setup Instructions/How to Run
To run the program, follow these steps:

1. Ensure you have Python 3 and virtualenv installed on your system (```pip3 install virtualenv```).
2. Change directory to the root `alphabet-soup` project on terminal
3. Create a new virtualenv with ```python3 -m venv myenv```
4. Activate the virtualenv with ```source myenv/bin/activate```
5. Run the file ```setup.py``` by executing ```pip install -e .``` to dynamically install all packages necessary to run all the files (e.g. pytest) from the root.
6. Run the script with one of the input files provided as an argument:
```python src/word_search_solver.py files/sample_input.txt```
7. Run unit tests from the root directory: ```pytest```

## Implementation Details

### Ternary Search Trie (TST)
The TST is a data structure used to store and search strings. Each node in the TST can have up to three children: left, middle, and right, corresponding to characters less than, equal to, and greater than the node's character, respectively. 

A Ternary Search Trie (TST) can be more efficient than a regular trie, particularly in terms of space usage, as it only creates nodes for characters present in the dataset, reducing memory consumption. In a regular trie, each node typically has as many children as there are possible characters (e.g., 26 for the English alphabet). 

TSTs offer faster access to less common characters and can handle variable-length strings more flexibly. They inherently maintain a balanced structure, providing consistent and efficient search performance. 

### Word Search Solver Logic
The solver (```word_search_solver.py```) `solve()` function reads the grid and the list of words from the input file, stores the words in the TST, and then searches for each word in the grid in all possible directions.

## Complexity Analysis

### Time Complexity
- **TST Construction**: O(M) for each word, where M is the length of the word.
- **Search**: O(N * M) for each word, where N is the number of cells in the grid.

### Space Complexity
- **TST**: O(W * M), where W is the number of words and M is the average length of the words.
- **Grid Storage**: O(N), where N is the number of cells in the grid.

## Solution Notes
My solution implements a custom data structure to optimize for time and space complexity. There are numerous tree structures that can be used for this solution, but a trie is the most optimal. Of the tries I decided to use a Ternary Search Trie. Regular tries can get bogged down with prefixes that aren't valid words, leading to unnecessary searching. Ternary search tries branch off nodes based on character validity, skipping dead ends and efficiently navigating valid word segments. And since we know which words we're looking for already having a map to the crossword solution instead of blindly wandering the puzzle, we can use a ternary search trie.

I wrote a very simple implementation of a TST based off the specification designed by Robert Sedgewick and Kevin Wayne in their textbook _Algorithims_ here: https://algs4.cs.princeton.edu/52trie/ (albeit their example is in Java and mine is in Python). To further enhance its functionality one could write custom path compression to optimize memory usage, where we merge multiple nodes with single children.

I also wrote a Utility class that holds "helper" functions, like one that simply sorts a list of words in ascending order by length, and a function that reads files given a filepath.

### Unit Tests
Tests can be run by running the command `pytest` from the `alphabet-soup` parent directory:

~~~bash
(.venv) macbookpro:alphabet-soup macbookpro$ pwd
/Users/macbookpro/Desktop/alphabet-soup
(.venv) macbookpro:alphabet-soup macbookpro$ ls
README.md       build           dist            files           myenv           setup.py        src             tests
(.venv) macbookpro:alphabet-soup macbookpro$ pytest
========================================================================================================== test session starts ==========================================================================================================
platform darwin -- Python 3.9.17, pytest-7.4.4, pluggy-1.3.0
rootdir: /Users/macbookpro/Desktop/alphabet-soup
collected 9 items                                                                                                                                                                                                                 
tests/word_search_solver_test.py .........                                                                                                                                                                                        [100%]
=========================================================================================================== 9 passed in 0.17s ===========================================================================================================
~~~

#### Advantages over Naive Implementation (Brute Force)
- **Efficient Search**: Unlike a naive implementation that might check every possible starting position and direction for each word when searching through a matrix or two dimensional array, the TST allows for a more efficient search by quickly eliminating impossible matches.
- **Word Length Prioritization**: Inserts shorter words first since probability states they are more likely to be found in the context of a wordsearch puzzle, thus decreasing the search space for other words.
- **Reduced Redundancy**: The TST stores common prefixes only once, reducing memory usage compared to storing each word independently.
- **Scalability**: The TST scales well with a large number of words, as the increase in search time and memory is moderate compared to a naive approach.
