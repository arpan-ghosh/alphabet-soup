import pytest
from pathlib import Path
from src.utility import Utility
from src.word_search_solver import WordSearchSolver


@pytest.fixture
# Fixtures to set up common test data
def sample_grid():
    return [
        ['A', 'B', 'C'],
        ['D', 'E', 'F'],
        ['G', 'H', 'I']
    ]


# Creating a global grid without pytest fixtures to display another way
SAMPLE_REAL_WORDS_GRID = [
    ['H', 'A', 'S', 'D', 'F'],
    ['G', 'E', 'Y', 'B', 'H'],
    ['J', 'K', 'L', 'Z', 'X'],
    ['C', 'V', 'B', 'L', 'N'],
    ['G', 'O', 'O', 'D', 'O']
]

SAMPLE_TEN_BY_TEN_GRID = [
    ["T", "R", "E", "E", "S", "A", "B", "C", "D", "H"],
    ["F", "G", "H", "I", "J", "K", "L", "M", "I", "O"],
    ["P", "Q", "R", "S", "T", "U", "V", "K", "X", "Y"],
    ["Z", "A", "B", "O", "A", "T", "E", "G", "H", "I"],
    ["J", "K", "L", "M", "N", "O", "P", "Q", "R", "S"],
    ["T", "U", "V", "W", "X", "Y", "Z", "A", "B", "C"],
    ["D", "E", "F", "A", "H", "P", "M", "A", "L", "M"],
    ["N", "O", "P", "T", "R", "S", "T", "U", "V", "W"],
    ["X", "Y", "Z", "E", "B", "C", "D", "E", "F", "G"],
    ["H", "I", "J", "R", "L", "M", "N", "O", "P", "Q"]
]


def test_find_word_horizontal(sample_grid: list[list[str]]):
    words = ['ABC', 'GHI']
    solver = WordSearchSolver(sample_grid, words)
    assert solver.find_word("ABC") == (0, 0, 0, 2)


def test_find_word_diagonal(sample_grid: list[list[str]]):
    words = ['ABC', 'AEI']
    solver = WordSearchSolver(sample_grid, words)
    assert solver.find_word("AEI") == (0, 0, 2, 2)


def test_find_word_vertical():
    words = ["HELLO", "GOOD", "BYE"]
    solver = WordSearchSolver(SAMPLE_REAL_WORDS_GRID, words)
    assert solver.find_word("HELLO") == (0, 0, 4, 4)


def test_find_word():
    # Set up your test data
    words = ["HELLO", "GOOD", "BYE"]
    solver = WordSearchSolver(SAMPLE_REAL_WORDS_GRID, words)

    # Example test: Check if the word "GOOD" is found correctly
    assert solver.find_word("GOOD") == (
        4, 0, 4, 3)


def test_word_not_found():
    # Set up your test data
    words = ["ENLIGHTEN"]
    solver = WordSearchSolver(SAMPLE_REAL_WORDS_GRID, words)

    # Example test: Check if the word "GOOD" is found correctly
    assert solver.find_word("ENLIGHTEN") == None


def test_words_extracted_from_file():
    words = ["TREES", "LAMP", "WATER", "HIKE", "BOAT"]
    directory = Path('files')
    file_name = 'sample_input_3.txt'
    file_path = directory / file_name

    _, file_words = Utility(file_path).read_word_search_file()
    assert file_words == words


def test_solve():
    directory = Path('files')
    file_name = 'sample_input_3.txt'
    file_path = directory / file_name

    grid, words = Utility(file_path).read_word_search_file()
    solver = WordSearchSolver(grid, words)
    assert solver.solve() != None


def test_read_word_search_file():
    directory = Path('files')
    file_name = 'sample_input.txt'
    file_path = directory / file_name

    grid, _ = Utility(file_path).read_word_search_file()
    assert grid == SAMPLE_REAL_WORDS_GRID


def test_read_ten_by_ten_word_search_file():
    directory = Path('files')
    file_name = 'sample_input_3.txt'
    file_path = directory / file_name

    grid, _ = Utility(file_path).read_word_search_file()
    assert grid == SAMPLE_TEN_BY_TEN_GRID
