from ternary_search_trie import TernarySearchTrie
from utility import Utility
import sys


class WordSearchSolver:
    """
    Initialize the WordSearchSolver class.

    :param grid: The word search grid.
    :param words: The list of words to search for in the grid.
    """

    def __init__(self, grid, words):
        self.grid = grid
        self.words = words
        self.trie = TernarySearchTrie()
        self.build_trie()

    def build_trie(self):
        """Efficient loading of words into the trie prioritizing shorter words first."""
        self.sorted_words = Utility.sort_words_by_length(self.words)
        self.add_words(self.sorted_words)

    def add_words(self, words):
        """
        Efficiently adds multiple words to the Trie.

        :param words: The list of words to add to the Trie.
        """
        for word in words:
            self.trie.put(word)

    def find_word(self, word):
        """
        Find a word in the grid and return their coordinates if found.

        :param word: The word to search for in the grid.
        :return: A tuple of coordinates (start_x, start_y, end_x, end_y) 
                 if the word is found.
        """
        grid_height = len(self.grid)
        grid_width = len(self.grid[0])

        return next((self.search_starting_from_coordinate(word, i, j)
                     for i in range(grid_height)
                     for j in range(grid_width)
                     if self.search_starting_from_coordinate(word, i, j) is not None), None)

    def search_starting_from_coordinate(self, word, start_x, start_y):
        """
        Search for a word starting from a given coordinate and direction.

        :param word: The word to be searched.
        :param x: The starting x-coordinate.
        :param y: The starting y-coordinate.
        :return: A tuple of coordinates (start_x, start_y, end_x, end_y) 
                 if the word is found in the direction, else None.
        """
        for dx, dy in self.directions():
            if self._search_direction(word, start_x, start_y, dx, dy):
                return (start_x, start_y, start_x + (len(word) - 1) * dx,
                        start_y + (len(word) - 1) * dy)
        return None

    @staticmethod
    def directions():
        """
        Generate a list of valid directions for searching.

        :return: List of direction tuples [(dx, dy)].
        """
        return [(dx, dy) for dx in [-1, 0, 1] for dy in [-1, 0, 1] if not (dx == 0 and dy == 0)]

    def _search_direction(self, word, start_x, start_y, dx, dy):
        """
        Search for a word in a specific direction starting from given coordinates.

        :param word: The word to search for.
        :param x: The starting x-coordinate.
        :param y: The starting y-coordinate.
        :param dx: The x-direction to search.
        :param dy: The y-direction to search.
        :return: True if the word is found in the direction, else False.
        """
        for i, char in enumerate(word):
            current_x, current_y = start_x + i * dx, start_y + i * dy
            if (
                current_x < 0
                or current_y < 0
                or current_x >= len(self.grid)
                or current_y >= len(self.grid[0])
                or self.grid[current_x][current_y] != char
            ):
                return False
        return True

    def solve(self):
        """
        Solve the word search puzzle and return a dictionary of word coordinates.

        :return: A dictionary with words as keys and their coordinates as values.
        """
        results_dict = {}
        for word in self.words:
            result = self.find_word(word)
            results_dict[word] = None
            if result:
                start_x, start_y, end_x, end_y = result
                results_dict[word] = {
                    "start_x": start_x,
                    "start_y": start_y,
                    "end_x": end_x,
                    "end_y": end_y
                }
        return results_dict


if __name__ == "__main__":
    if len(sys.argv) > 1:
        file_name = sys.argv[1]
        utility = Utility(file_name)
        grid, words = utility.read_word_search_file()
    else:
        print("Please provide a file name as an argument.")

    solver = WordSearchSolver(grid, words)
    results = solver.solve()

    for word, coordinates in results.items():
        if coordinates:  # Check if the dictionary exists
            print(
                f"{word} {coordinates['start_x']}:{coordinates['start_y']} {coordinates['end_x']}:{coordinates['end_y']}")
        else:
            print(f"{word} not found")
