class TSTNode:
    """
    A node in a Ternary Search Tree (TST).

    TSTs are space-efficient trees for storing strings. Each node holds a single character.

    Attributes:
        char (str): Character stored in this node.
        left (TSTNode): Left child node (characters less than this node's char).
        middle (TSTNode): Middle child node (next character in the string).
        right (TSTNode): Right child node (characters greater than this node's char).
        end_of_word (bool): Flag to indicate if node is the end of a word.
    """

    def __init__(self, char):
        """
        Initializes a new instance of the TSTNode class.

        :param char: The character to be stored in this node. Typically, this character 
                     is a part of a string (or word) being inserted into the TST.
        """
        self.char = char
        self.left = None    # Initialize the left child to None
        self.middle = None  # Initialize the middle child to None
        self.right = None   # Initialize the right child to None
        self.end_of_word = False  # Initially, the node is not the end of a word
