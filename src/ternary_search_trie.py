from tst_node import TSTNode


class TernarySearchTrie:
    def __init__(self):
        """
        Initialize an empty Ternary Search Trie (TST).

        The Ternary Search Trie is a tree-based data structure used
        for efficient string storage and retrieval.
        """
        self.root = None

    def put(self, word):
        """
        Insert a word into the Ternary Search Trie.

        :param word: The word to insert into the trie.
        """
        self.root = self._put(self.root, word, 0)

    def _put(self, node, word, d):
        """
        Recursive helper function to insert a word into the Ternary Search Trie.

        :param node: The current node being processed.
        :param word: The word to insert.
        :param d: The current character index in the word being processed.
        :return: The updated node after insertion.
        """
        char = word[d]
        if node is None:
            node = TSTNode(char)
        if char < node.char:
            node.left = self._put(node.left, word, d)
        elif char > node.char:
            node.right = self._put(node.right, word, d)
        elif d < len(word) - 1:
            node.middle = self._put(node.middle, word, d+1)
        else:
            node.end_of_word = True
        return node

    def search(self, word):
        """
        Search for a word in the Ternary Search Trie.

        :param word: The word to search for.
        :return: True if the word is found, False otherwise.
        """
        return self._search(self.root, word, 0)

    def _search(self, node, word, d):

        if node is None:
            return False
        char = word[d]
        if char < node.char:
            return self._search(node.left, word, d)
        elif char > node.char:
            return self._search(node.right, word, d)
        elif d < len(word) - 1:
            return self._search(node.middle, word, d+1)
        else:
            return node.end_of_word
