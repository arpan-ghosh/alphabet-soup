import re


class Utility:
    """Utility class for helper functions and handling word search file operations."""

    def __init__(self, file_path):
        self.file_path = file_path

    def read_word_search_file(self):
        """
        Read the word search file, parse the grid size and words.

        :return: A tuple containing the grid and words.
        """
        with open(self.file_path, 'r') as file:
            # Read the first line and parse the grid size from nxn format
            rows, cols = map(int, file.readline().strip().split('x'))

            # Read and validate the grid
            grid = []
            for _ in range(rows):
                row = file.readline().split()
                if len(row) != cols:
                    raise ValueError(
                        f"Row does not have the correct number of columns (expected {cols}, got {len(row)})")
                grid.append(row)

            # Read the remaining lines as words to be found in the grid
            words = [line.strip() for line in file]

        return grid, words

    @staticmethod
    def validate(file_path):
        """
        Validate the format of the word search file.

        :param file_path: The path to the word search file.
        :return: A file object if the format is correct, or an error message if it's incorrect.
        """
        # Regex: one or more digits, followed by 'x', followed by one or more digits
        pattern = r'^\d+x\d+$'

        with open(file_path, 'r') as file:
            first_line = file.readline().strip()  # Read the first line and strip whitespace

        # Check if the first line matches the pattern
        if not re.match(pattern, first_line):
            return "File format is incorrect. Please input a proper file."
        else:
            return file

    @staticmethod
    def sort_words_by_length(words):
        """
        Sort a list of words by their length in ascending order.

        :param words: The list of unsorted words.
        :return: A sorted list of words.
        """
        return sorted(words, key=len)
