# conftest.py

import sys
import os

# Append the 'src' folder to Python's path so modules
# can be found when running pytest from root
sys.path.append(os.path.abspath('src'))
