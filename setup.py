from setuptools import setup, find_packages

setup(
    name='alphabet-soup-solver',
    version='1.0',
    packages=find_packages(where='src'),
    install_requires=[
        'pathlib',
        'pytest'
    ],
    package_dir={'': 'src'}
)
